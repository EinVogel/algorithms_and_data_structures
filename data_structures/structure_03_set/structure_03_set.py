data_set = {1, 2, 3}
# len	O(1)
len(data_set)
# .add	O(1)
data_set.add(5)
# Проверка на входимость	O(1)
print(1 in data_set)
# .remove	O(1)
data_set.remove(1)
# Убрать элемент из множества	O(1)
data_set.discard(1)
# .pop	O(1)
data_set.pop()
# .clear	O(1)
# Создание	O(len(t))	set(t)
# Проверки ==, !=	O((len(s))	s != t

# .issubset	O(len(s))	s.issubset(t)
A = {'a', 'c', 'e'}
B = {'a', 'b', 'c', 'd', 'e'}
# Returns True
print('A is subset of B:', A.issubset(B))
# Returns False
print('B is subset of A:', B.issubset(A))

# .issuperset	O(len(t))	s.issuperset(t)
A = {1, 2, 3, 4, 5}
B = {1, 2, 3}
C = {1, 2, 3}
# Returns True
print(A.issuperset(B))
# Returns False
print(B.issuperset(A))
# Returns True
print(C.issuperset(B))

# .union	O(len(s)+len(t))	s.union(t)
A = {2, 3, 5}
B = {1, 3, 5}
# compute union between A and B
print('A U B = ', A.union(B))
# Output: A U B =  {1, 2, 3, 5}

# .intersection	O(min(len(s), len(t)))	s.intersection(t)	Заменить min на max, если t не множество.
A = {2, 3, 5}
B = {1, 3, 5}
# compute intersection between A and B
print(A.intersection(B))
# Output: {3, 5}

# .difference	O(len(s))	s.difference(t)
# sets of numbers
A = {1, 3, 5, 7, 9}
B = {2, 3, 5, 7, 11}
# returns items present only in set A
print(A.difference(B))
# Output:  {1, 9}

# .symmetric_difference	O(len(s))	s ^ t
A = {'a', 'b', 'c', 'd'}
B = {'c', 'd', 'e' }
# returns all items to result variable except the items on intersection
result = A.symmetric_difference(B)
print(result)
# Output: {'a', 'b', 'e'}

# Проход	O(n)	for v in s:

# .copy	O(n)	s.copy()
