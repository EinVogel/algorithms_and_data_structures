data_list = [i for i in range(10)]
# Обращение по индексу	O(1)
print(data_list[1])
# Присвоение	O(1)
data_list[1] = 10
# Длина списка	O(1)
print(len(data_list))
# Добавление в конец списка	O(1)
data_list.append(10)
# Pop с конца	O(1)
data_list.pop()
# Очистка списка O(1)
# data_list.clear()
# Срез	O(b-a)	l[a:b]
print(data_list[2:4])
# .extend	O(k), k - 3 элемента [1, 2, 3]
data_list_2 = [1, 2, 3]
data_list.extend(data_list_2)
# Создание	O(k)
data = (1, 2, 3)
data = list(data)
# Проверки ==, !=	O(n)
print(data == data_list)
# .insert	O(n)
data_list.insert(0, 5)
# del	O(n)
del data_list[0]
# .remove	O(n)	l.remove(...)
data_list.remove(9)
print(data_list)
# Проверка на входимость
print(3 in data)
# .copy	O(n)	l.copy()	То же что и l[:].
print(data_list.copy())
# .pop	O(n)
data_list.pop(1)
# min, max	O(n)
min(data_list)
# .reverse	O(n)
data_list.reverse()
# Проход	O(n)	for v in l:
print([i for i in data_list])
# Присвоение среза	O(k+n)	l[... : ...] = ...
# .sort	O(n log n)	l.sort()
# Умножение	O(k×n)	i l
print(data_list * 3)

