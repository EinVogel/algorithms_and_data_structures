"""
Обращение по индексу	O(1)	d[k]	
Присвоение	O(1)	d[k] = v	
len	O(1)	len(d)	
del	O(1)	del d[k]	
.setdefault	O(1)	d.setdefault(1)	
.pop	O(1)	d.pop(k)	
.popitem	O(1)	d.popitem()	
.clear	O(1)	d.clear()	То же: d = {} и d = dict().
Представление	O(1)	d.keys()	
Создание	O(k)	dict(obj)	Зависит от числа кортежей (ключ, значение).
Проход	O(n)	for k in d:	То же для keys(), values(), items().
.copy	O(n)	d1 = d.copy()"""

data_dict = {"a": 1, "b": 2}

# Обращение по индексу	O(1)	d[k]
print(data_dict['a'])
# Присвоение	O(1)	d[k] = v
data_dict['a'] = 3
# len	O(1)	len(d)
print(len(data_dict))
# del	O(1)	del d[k]
del data_dict['a']
# .setdefault	O(1)	d.setdefault(1)
my_dict = {'a': 1}
my_dict.setdefault('a', 2)  # 1
# {'a': 1}
my_dict.setdefault('b', 2)  # 2
# {'a': 1, 'b': 2}
# .pop	O(1)	d.pop(k)
data_dict.pop('a')
# .popitem	O(1)	d.popitem()
data_dict.popitem()
# .clear	O(1)	d.clear()	То же: d = {} и d = dict().
# Представление	O(1)	d.keys()
# Создание	O(k)	dict(obj)	Зависит от числа кортежей (ключ, значение).
# Проход	O(n)	for k in d:	То же для keys(), values(), items().
# .copy	O(n)	d1 = d.copy()
