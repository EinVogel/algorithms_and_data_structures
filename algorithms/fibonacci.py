
def get_fibonacci_number_with_recursive(n: int) -> int:
    """Рекурсивный алгоритм"""
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n == 2:
        return 1
    return get_fibonacci_number_with_recursive(n - 1) + get_fibonacci_number_with_recursive(n - 2)


print(get_fibonacci_number_with_recursive(2))


def get_fibonacci_number_with_no_recursive(n: int) -> int:
    """Нерекурсивный алгоритм"""
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n == 2:
        return 1
    previous_number = 1
    last_number = 1
    result = 0
    for iteration in range(n - 2):
        result = previous_number + last_number
        previous_number = last_number
        last_number = result
    return result


print(get_fibonacci_number_with_no_recursive(2))

