from math import sqrt


def is_simple_number(n: int) -> bool:
    """Является ли число простым"""
    if n == 1:
        return False
    max_divider = int(sqrt(n))
    for divider in range(2, max_divider + 1):
        if n % divider == 0:
            return False
    return True


print(is_simple_number(1))

