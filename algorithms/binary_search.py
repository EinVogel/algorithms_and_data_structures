def binary_search(n: int, array: list):
    array.sort()
    start = 0
    end = len(array)
    step = 0
    while start <= end:
        print("Subarray in step {}: {}".format(step, str(array[start:end + 1])))
        step = step + 1
        mid = (start + end) // 2

        if n == array[mid]:
            return mid

        if n < array[mid]:
            end = mid - 1
        else:
            start = mid + 1
    return -1


print(binary_search(3, [1, 2, 3, 4, 5, 6, 7, 8]))
